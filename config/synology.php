<?php

return [
    'proto' => env('SYNO_PROTO', 'http'),
    'host' => env('SYNO_HOST'),
    'port' => env('SYNO_PORT', 5000),
    'user' => env('SYNO_USER'),
    'pass' => env('SYNO_PASS'),
];
