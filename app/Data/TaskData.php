<?php

namespace App\Data;

class TaskData
{
    public const STATUS_WAITING = 'waiting';

    public const STATUS_DOWNLOADING = 'downloading';

    public const STATUS_PAUSED = 'paused';

    public const STATUS_FINISHING = 'finishing';

    public const STATUS_FINISHED = 'finished';

    public const STATUS_HASH_CHECKING = 'hash_checking';

    public const STATUS_SEEDING = 'seeding';

    public const STATUS_FILEHOSTING_WAITING = 'filehosting_waiting';

    public const STATUS_EXTRACTING = 'extracting';

    public const STATUS_ERROR = 'error';

    public function __construct(
        public readonly string $id,
        public readonly string $title,
        public readonly int $size,
        public readonly int $size_downloaded,
        public readonly int $size_uploaded,
        public readonly int $speed_download,
        public readonly int $speed_upload,
        public readonly string $status,
    ) {
    }
}
