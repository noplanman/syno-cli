<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Synology_AudioStation_Api;
use Synology_DownloadStation_Api;
use Synology_DSM_Api;
use Synology_DTV_Api;
use Synology_FileStation_Api;
use Synology_VideoStation_Api;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        foreach ([
            Synology_AudioStation_Api::class,
            Synology_DownloadStation_Api::class,
            Synology_DSM_Api::class,
            Synology_DTV_Api::class,
            Synology_FileStation_Api::class,
            Synology_VideoStation_Api::class,
        ] as $api) {
            $this->app->singleton($api, function () use ($api) {
                $synology = new $api(config('synology.host'), config('synology.port'), config('synology.proto'), 1);
                $synology->connect(config('synology.user'), config('synology.pass'));

                return $synology;
            });
        }
    }
}
