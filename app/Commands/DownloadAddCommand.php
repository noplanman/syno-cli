<?php

namespace App\Commands;

use Synology_Exception;

use function Laravel\Prompts\alert;
use function Laravel\Prompts\info;
use function Laravel\Prompts\spin;
use function Laravel\Prompts\text;

class DownloadAddCommand extends SynoCommand
{
    protected $signature = 'download:add';

    protected $description = 'Add download task';

    public function handle()
    {
        while ($uri = text('Enter URI to download')) {
            $this->add($uri);
        }
    }

    protected function add(string $uri): void
    {
        $this->connect();

        $response = spin(function () use ($uri) {
            try {
                return $this->addTask($uri);
            } catch (Synology_Exception $e) {
                return $e->getMessage();
            }
        }, 'adding download task...');

        if ($response !== true) {
            alert($response ?: 'Failed to add download task');

            return;
        }

        info('Download added!');
    }
}
