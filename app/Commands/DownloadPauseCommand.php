<?php

namespace App\Commands;

use function Laravel\Prompts\error;
use function Laravel\Prompts\info;
use function Laravel\Prompts\spin;
use function Laravel\Prompts\warning;

class DownloadPauseCommand extends SynoCommand
{
    protected $signature = 'download:pause
                            {id? : Task ID (optional)}
                            {--a|all : All tasks (optional)}';

    protected $description = 'Pause download task(s)';

    public function handle()
    {
        $ids = $this->loadIds();
        $all = $this->option('all');

        if (! $ids && ! $all) {
            error('Pass ids or "--all"');

            return;
        }

        $this->connect();
        $tasks = $this->fetchTasks();

        foreach ($tasks as $task) {
            if (! $all && ! in_array($task->id, $ids, true)) {
                continue;
            }

            spin(
                fn () => $this->pauseTask($task), "pausing {$task->id}: {$task->title}"
            ) || warning("Failed to pause {$task->id}: {$task->title}");
        }

        info('Finished');
    }
}
