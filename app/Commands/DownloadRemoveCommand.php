<?php

namespace App\Commands;

use App\Data\TaskData;

use function Laravel\Prompts\error;
use function Laravel\Prompts\info;
use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\spin;
use function Laravel\Prompts\warning;

class DownloadRemoveCommand extends SynoCommand
{
    protected $signature = 'download:remove
                            {id?* : Task ID (optional)}
                            {--s|status=*' . TaskData::STATUS_FINISHED . ',' . TaskData::STATUS_SEEDING . ' : Only tasks with the passed status (optional)}
                            {--a|all : All tasks (optional)}';

    protected $description = 'Delete download task(s)';

    public function handle()
    {
        $ids = $this->loadIds();
        $all = $this->option('all');

        $status = $this->optionAsArray('status');

        if (! $ids && ! $all && ! $status) {
            error('Pass ids, "--status=<status>" or "--all"');

            return;
        }

        $this->connect();
        $tasks = $this->fetchTasks();

        $tasks_to_remove = $tasks->filter(
            static fn ($task) => $all || in_array($task->status, $status, true) || in_array($task->id, $ids, true)
        );

        if ($tasks_to_remove->isEmpty()) {
            warning('No tasks meet your criteria.');

            return;
        }

        $multiselect_options = $tasks_to_remove->mapWithKeys(
            fn ($task) => [$task->id => "{$task->id} [{$task->status}] {$task->title}"]
        );

        $tasks_to_remove->whereIn('id', multiselect(
            label: 'Tasks to be removed',
            options: $multiselect_options,
            default: $multiselect_options->keys()
        ))->each(function ($task) {
            spin(
                fn () => $this->removeTask($task), "removing {$task->id}: {$task->title}"
            ) || warning("Failed to remove {$task->id}: {$task->title}");
        });

        info('Finished');
    }
}
