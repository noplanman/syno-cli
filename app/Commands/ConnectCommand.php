<?php

namespace App\Commands;

use function Laravel\Prompts\confirm;
use function Laravel\Prompts\info;
use function Laravel\Prompts\password;
use function Laravel\Prompts\select;
use function Laravel\Prompts\text;

class ConnectCommand extends SynoCommand
{
    protected $signature = 'connect';

    protected $description = 'Connect to Synology Disk Station';

    protected string $env_file;

    public function handle()
    {
        $this->env_file = $this->laravel->environmentFilePath();

        config([
            'synology.proto' => $proto = select('Protocol', ['http', 'https'], 'https'),
            'synology.host' => $host = text('Hostname or IP address', required: 'Where does your Disk Station live?'),
            'synology.port' => $port = text('Port', $proto === 'http' ? 5000 : 5001, '', true, fn (string $value) => ! is_numeric($value) ? 'Port must be a number' : null),
            'synology.user' => $user = text('Username', required: 'Who do you want to connect as?'),
            'synology.pass' => $pass = password('Password', required: 'Hopefully only you know this.'),
        ]);

        $this->connect();

        info('Successfully connected!');

        if (confirm("Save these credentials to your .env file? [{$this->env_file}]", false)) {
            foreach ([
                'SYNO_PROTO' => $proto,
                'SYNO_HOST' => $host,
                'SYNO_PORT' => $port,
                'SYNO_USER' => $user,
                'SYNO_PASS' => $pass,
            ] as $key => $value) {
                $this->writeEnvironmentFileWith($key, $value);
            }

            info('Saved!');
        }
    }

    protected function writeEnvironmentFileWith(string $key, string $value): void
    {
        $value = str_replace('"', '\"', $value);

        $replaced = preg_replace(
            "/^{$key}=.*/m",
            $row = "{$key}=\"{$value}\"",
            file_get_contents($this->env_file),
            -1,
            $count
        );

        $count > 0 || $replaced .= "\n{$row}";

        file_put_contents($this->env_file, $replaced);
    }
}
