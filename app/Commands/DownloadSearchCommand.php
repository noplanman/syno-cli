<?php

namespace App\Commands;

use Illuminate\Support\Collection;
use OwenVoke\YTS\Movie;
use OwenVoke\YTS\Movies;
use OwenVoke\YTS\Torrent;
use Throwable;

use function Laravel\Prompts\search;
use function Laravel\Prompts\select;

class DownloadSearchCommand extends DownloadAddCommand
{
    protected $signature = 'download:search';

    protected $description = 'Search download task';

    public function handle()
    {
        while (true) {
            $this->search();
        }
    }

    protected function search(): void
    {
        $movie_id = search(
            label: 'Search for movie on YTS',
            options: fn (string $value) => $value !== ''
                ? $this->movies($value)->mapWithKeys(function (Movie $movie) {
                    $desc = rtrim(" » {$movie->getDescriptionFull()}", ' »');

                    return [$movie->getId() => "{$movie->getYear()} {$movie->getTitleEnglish()}{$desc}"];
                })->all()
                : [],
        );

        /** @var Movie $movie */
        $movie = cache()->get("yts:{$movie_id}");

        $torrent_hashes = $movie->getTorrents()->mapWithKeys(
            fn (Torrent $torrent) => [$torrent->getHash() => $torrent->getQuality() . ' @ ' . $torrent->getSize() . " | {$torrent->getSeeds()} seeds, {$torrent->getPeers()} peers"],
        )->all();

        $torrent_hash = select(
            label: 'Select torrent',
            options: $torrent_hashes,
        );

        $torrent_url = $movie->getTorrents()->firstWhere(fn (Torrent $torrent) => $torrent->getHash() === $torrent_hash)->getUrl();

        $this->add($torrent_url);
    }

    /**
     * @return Collection<Movie>
     */
    protected function movies(string $search): Collection
    {
        /** @var Movie[] $movies */
        $movies = cache()->remember(strtolower("search:{$search}"), 3600, function () use ($search) {
            try {
                return Movies::list([
                    'query_term' => $search,
                    'minimum_rating' => 0,
                    'quality' => Movies::QUALITY_ALL,
                    'sort_by' => 'seeds',
                    'order_by' => 'desc',
                ]);
            } catch (Throwable) {
                return collect();
            }
        });

        foreach ($movies as $movie) {
            cache()->remember("yts:{$movie->getId()}", 3600, fn () => $movie);
        }

        return $movies;
    }
}
