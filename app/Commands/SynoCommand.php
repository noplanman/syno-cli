<?php

namespace App\Commands;

use App\Data\TaskData;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;
use Synology_DownloadStation_Api;
use Synology_Exception;

use function Laravel\Prompts\alert;
use function Laravel\Prompts\spin;

abstract class SynoCommand extends Command
{
    protected static function humanFilesize(int $size, int $precision = 2): string
    {
        for ($i = 0; ($size / 1024) > 0.9; $i++, $size /= 1024) {
        }

        return round($size, $precision) . ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'][$i];
    }

    protected function argumentAsArray(string $key): array
    {
        $values = array_map(static fn (string $value) => explode(',', $value), (array) ($this->argument($key) ?? []));

        return array_filter(array_unique(Arr::flatten($values)));
    }

    protected function optionAsArray(string $key): array
    {
        $values = array_map(static fn (string $value) => explode(',', $value), (array) ($this->option($key) ?? []));

        return array_filter(array_unique(Arr::flatten($values)));
    }

    protected function loadIds(): array
    {
        return array_map(
            static fn (string $id) => 'dbid_' . str_replace('dbid_', '', $id),
            $this->argumentAsArray('id')
        );
    }

    protected function connect(): Synology_DownloadStation_Api|string
    {
        try {
            spin(
                fn () => ! empty(app(Synology_DownloadStation_Api::class)),
                sprintf('connecting to %s@%s:%d', config('synology.user'), config('synology.host'), config('synology.port')),
            ) || alert('Failed to connect.');

            return app(Synology_DownloadStation_Api::class);
        } catch (Exception $e) {
            alert($e->getMessage());
            exit;
        }
    }

    /**
     * @return TaskData[]|Collection
     */
    protected function fetchTasks(): array|Collection
    {
        return spin(function () {
            try {
                // $tasks = collect(app(Synology_DownloadStation_Api::class)->getTaskList(additional: ['detail', 'transfer', 'file', 'tracker', 'peer'])->tasks);
                return collect(app(Synology_DownloadStation_Api::class)
                    ->getTaskList(additional: ['transfer'])?->tasks ?? [])
                    ->map(fn ($task) => new TaskData(
                        $task->id,
                        $task->title,
                        $task->size,
                        $task->additional->transfer->size_downloaded,
                        $task->additional->transfer->size_uploaded,
                        $task->additional->transfer->speed_download,
                        $task->additional->transfer->speed_upload,
                        $task->status
                    ));
            } catch (Synology_Exception $e) {
                alert($e->getMessage());
            }

            return collect();
        }, 'fetching tasks...');
    }

    protected function addTask(string $uri): bool
    {
        if (empty($uri)) {
            return false;
        }

        try {
            app(Synology_DownloadStation_Api::class)->addTask($uri);

            return true;
        } catch (Synology_Exception) {
            return false;
        }
    }

    protected function pauseTask(TaskData $task): bool
    {
        if ($task->status === TaskData::STATUS_PAUSED) {
            return true;
        }

        try {
            app(Synology_DownloadStation_Api::class)->pauseTask($task->id);

            return true;
        } catch (Synology_Exception) {
            return false;
        }
    }

    protected function resumeTask(TaskData $task): bool
    {
        if ($task->status !== TaskData::STATUS_PAUSED) {
            return true;
        }

        try {
            app(Synology_DownloadStation_Api::class)->resumeTask($task->id);

            return true;
        } catch (Synology_Exception) {
            return false;
        }
    }

    protected function removeTask(TaskData $task): bool
    {
        try {
            app(Synology_DownloadStation_Api::class)->deleteTask($task->id);

            return true;
        } catch (Synology_Exception) {
            return false;
        }
    }
}
