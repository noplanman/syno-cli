<?php

namespace App\Commands;

use App\Data\TaskData;

use function Laravel\Prompts\table;
use function Laravel\Prompts\warning;

class DownloadListCommand extends SynoCommand
{
    protected $signature = 'download:list';

    protected $description = 'List all download tasks';

    public function handle()
    {
        $this->connect();
        $tasks = $this->fetchTasks();

        if ($tasks->isEmpty()) {
            warning('No tasks found.');

            return;
        }

        table(
            ['ID', 'Title', 'Downloaded', 'Uploaded', 'Total', 'Done %', 'Down', 'Up', 'Status'],
            $tasks->map(fn (TaskData $task) => [
                $task->id,
                $task->title,
                self::humanFilesize($task->size_downloaded),
                self::humanFilesize($task->size_uploaded),
                self::humanFilesize($task->size),
                $task->size ? round($task->size_downloaded / $task->size * 100, 2) : 'n/a',
                self::humanFilesize($task->speed_download),
                self::humanFilesize($task->speed_upload),
                $task->status,
            ])
        );
    }
}
